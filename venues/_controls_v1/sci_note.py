



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../modules',
	'../modules_pip'
])


import apoplast.measures.number.sci_note as sci_note

print (dir (sci_note))

print ("sci_note:", sci_note)
assert (sci_note.calc ('9999999') == [ "9.999", "e+6" ]), sci_note.calc ('9999999')

