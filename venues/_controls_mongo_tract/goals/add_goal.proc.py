




#----
#
import pathlib
from os.path import dirname, join, normpath
import sys
import os

this_directory = pathlib.Path (__file__).parent.resolve ()	
def add_paths_to_system (paths):
	for path in paths:
		sys.path.insert (0, normpath (join (this_directory, path)))

add_paths_to_system ([
	'../../stages',
	'../../stages_pip'
])
#
#----

#----
#
from goodest.adventures.monetary.DB.goodest_tract.goals.insert import insert_goals_document
import goodest.goals.umuntu.FDA as FDA_goals_for_umuntu
#
#
import rich
#
#----

goal = FDA_goals_for_umuntu.retrieve ()

print ("adding goal")

insert_goals_document ({
	"document": goal
})
