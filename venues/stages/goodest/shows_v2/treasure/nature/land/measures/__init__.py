

'''
	from goodest.shows_v2.treasure.nature.land.measures import build_land_measures_foundation
	build_land_measures_foundation ()
'''


def build_land_measures_foundation ():
	return {
		"mass + mass equivalents": {
			"per recipe": {
				"grams": {
					"fraction string": "0"
				}
			}
		},
		"energy": {
			"per recipe": {
				"food calories": {
					"fraction string": "0"
				}
			}
		}
	}