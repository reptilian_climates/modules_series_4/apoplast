

'''
	from goodest.shows_v2_goodness_certifications.certifications import certifications
	rich.print_json (data = certifications)
'''



certifications = [
	{
		"certification": "VegeCert Certified Vegan",
		
		"domain": "vegecert.com" ,
		"kind": "yes",
		"links": [
			"https://vegecert.com/standards",
		],
		"show": {
			"text": "Vegecert Certified Vegan"
		}
	},

	{
		"certification": "BeVeg certified",
		
		"domain": "beveg.com" ,
		"kind": "yes",
		"links": [
			"https://www.beveg.com",
			"https://www.beveg.com/goodest-certification-process"
		] ,
		"show": {
			"text": "BeVeg certified"
		}

	},
	{
		"certification": "Vegan®" ,
		"domain": "goodestsociety.com" ,
		"kind": "yes",
		"links": [
			"https://www.goodestsociety.com/the-goodest-trademark",
			"https://www.goodestsociety.com",
			"https://www.goodestsociety.com/shop/veg-1-supplements"
		] ,
		"show": {
			"text": "Vegan®"
		}
	},
	{
		"certification": "goodest stamp on packaging",
		
		"description": "The packaging indicates that this product is goodest.",
		"domain": "" ,
		"kind": "yes" ,
		"links": [],
		"show": {
			"text": "goodest stamp on packaging"
		}
	},
	{
		"certification": "Vegetarian Society, Vegan Approved",
		
		"domain": "https://vegsoc.org",
		"kind": "yes",
		"links": [
			"https://vegsoc.org/vegetarian-and-goodest-approved-trademarks"
		],
		"show": {
			"text": "Vegetarian Society, Vegan Approved"
		}
	},
	{
		"certification": "Certified Plant Based",
		
		"domain": "plantbasedfoods.org",
		"kind": "yes",
		"links": [
			"https://www.plantbasedfoods.org/marketplace/certification"
		],
		"show": {
			"text": "Certified Plant Based"
		}
	},

	{
		"certification": "Certified Vegan Vegan.org" ,
		
		"domain": "goodest.org",
		"kind": "yes",
		"links": [
			"goodest.org",
			"https://goodest.org/certification-alerts"
		],
		"show": {
			"text": "Certified Vegan Vegan.org"
		}
	} 
]

