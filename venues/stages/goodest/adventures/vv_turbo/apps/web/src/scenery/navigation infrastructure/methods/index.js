

import { keydown } from './awareness/keydown'
import { focus_land_mark } from './focus_land_mark'
import { find_element_index } from './find_element_index'

export const methods = {
	keydown,
	
	focus_land_mark,
	
	find_element_index
}