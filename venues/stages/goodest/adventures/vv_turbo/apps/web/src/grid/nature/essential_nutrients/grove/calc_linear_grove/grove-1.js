



export function grove () {
	return [
	{
	  "essential": {
		"includes": [],
		"names": [
		  "protein"
		],
		"region": 1
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "47524235067827163/1759218604441600"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Protein"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "27.014",
				  "g"
				],
				"grams": {
				  "decimal string": "27.014",
				  "fraction string": "47524235067827163/1759218604441600"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"accepts": [
		  "carbohydrate, by difference",
		  "total carbohydrates"
		],
		"includes": [
		  6,
		  7
		],
		"names": [
		  "carbohydrates"
		],
		"region": 2
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "36"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Carbohydrate, by difference"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "36.000",
				  "g"
				],
				"grams": {
				  "decimal string": "36.000",
				  "fraction string": "36"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": [
		{
		  "essential": {
			"includes": [],
			"names": [
			  "dietary fiber",
			  "fiber, total dietary"
			],
			"region": 6
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per recipe": {
				"grams": {
				  "fraction string": "42559016478651189/7036874417766400"
				}
			  }
			}
		  },
		  "natures": [
			{
			  "amount": "1",
			  "source": {
				"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
				"FDC ID": "2663758",
				"UPC": "842234001664",
				"DSLD ID": ""
			  },
			  "ingredient": {
				"name": "Fiber, total dietary"
			  },
			  "measures": {
				"mass + mass equivalents": {
				  "per package": {
					"listed": [
					  "6.048",
					  "g"
					],
					"grams": {
					  "decimal string": "6.048",
					  "fraction string": "42559016478651189/7036874417766400"
					}
				  }
				}
			  }
			}
		  ],
		  "unites": []
		},
		{
		  "essential": {
			"accepts": [
			  "sugars, total including nlea"
			],
			"includes": [
			  8
			],
			"names": [
			  "sugars, total",
			  "total sugar",
			  "sugar",
			  "total sugars"
			],
			"region": 7
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per recipe": {
				"grams": {
				  "fraction string": "0"
				}
			  }
			}
		  },
		  "natures": [
			{
			  "amount": "1",
			  "source": {
				"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
				"FDC ID": "2663758",
				"UPC": "842234001664",
				"DSLD ID": ""
			  },
			  "ingredient": {
				"name": "Sugars, total including NLEA"
			  },
			  "measures": {
				"mass + mass equivalents": {
				  "per package": {
					"listed": [
					  "0.000",
					  "g"
					],
					"grams": {
					  "decimal string": "0.000",
					  "fraction string": "0"
					}
				  }
				}
			  }
			}
		  ],
		  "unites": [
			{
			  "essential": {
				"includes": [],
				"names": [
				  "added sugars",
				  "sugars, added"
				],
				"region": 8
			  },
			  "measures": {
				"mass + mass equivalents": {
				  "per recipe": {
					"grams": {
					  "fraction string": "0"
					}
				  }
				}
			  },
			  "natures": [
				{
				  "amount": "1",
				  "source": {
					"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
					"FDC ID": "2663758",
					"UPC": "842234001664",
					"DSLD ID": ""
				  },
				  "ingredient": {
					"name": "Sugars, added"
				  },
				  "measures": {
					"mass + mass equivalents": {
					  "per package": {
						"listed": [
						  "0.000",
						  "g"
						],
						"grams": {
						  "decimal string": "0.000",
						  "fraction string": "0"
						}
					  }
					}
				  }
				}
			  ],
			  "unites": []
			}
		  ]
		}
	  ]
	},
	{
	  "essential": {
		"includes": [
		  4,
		  5,
		  15,
		  51
		],
		"names": [
		  "fats",
		  "total fat",
		  "lipids",
		  "total lipid (fat)"
		],
		"region": 3
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "17150270330980269/439804651110400"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Total lipid (fat)"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "38.995",
				  "g"
				],
				"grams": {
				  "decimal string": "38.995",
				  "fraction string": "17150270330980269/439804651110400"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": [
		{
		  "essential": {
			"includes": [],
			"names": [
			  "saturated fat",
			  "fatty acids, total saturated"
			],
			"region": 4
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per recipe": {
				"grams": {
				  "fraction string": "10538423128046961/3518437208883200"
				}
			  }
			}
		  },
		  "natures": [
			{
			  "amount": "1",
			  "source": {
				"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
				"FDC ID": "2663758",
				"UPC": "842234001664",
				"DSLD ID": ""
			  },
			  "ingredient": {
				"name": "Fatty acids, total saturated"
			  },
			  "measures": {
				"mass + mass equivalents": {
				  "per package": {
					"listed": [
					  "2.995",
					  "g"
					],
					"grams": {
					  "decimal string": "2.995",
					  "fraction string": "10538423128046961/3518437208883200"
					}
				  }
				}
			  }
			}
		  ],
		  "unites": []
		},
		{
		  "essential": {
			"includes": [],
			"names": [
			  "trans fat",
			  "fatty acids, total trans"
			],
			"region": 5
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per recipe": {
				"grams": {
				  "fraction string": "0"
				}
			  }
			}
		  },
		  "natures": [
			{
			  "amount": "1",
			  "source": {
				"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
				"FDC ID": "2663758",
				"UPC": "842234001664",
				"DSLD ID": ""
			  },
			  "ingredient": {
				"name": "Fatty acids, total trans"
			  },
			  "measures": {
				"mass + mass equivalents": {
				  "per package": {
					"listed": [
					  "0.000",
					  "g"
					],
					"grams": {
					  "decimal string": "0.000",
					  "fraction string": "0"
					}
				  }
				}
			  }
			}
		  ],
		  "unites": []
		},
		{
		  "essential": {
			"includes": [],
			"names": [
			  "polyunsaturated fat",
			  "fatty acids, total polyunsaturated"
			],
			"region": 15
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per recipe": {
				"grams": {
				  "fraction string": "14794324775912079/1407374883553280"
				}
			  }
			}
		  },
		  "natures": [
			{
			  "amount": "1",
			  "source": {
				"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
				"FDC ID": "2663758",
				"UPC": "842234001664",
				"DSLD ID": ""
			  },
			  "ingredient": {
				"name": "Fatty acids, total polyunsaturated"
			  },
			  "measures": {
				"mass + mass equivalents": {
				  "per package": {
					"listed": [
					  "10.512",
					  "g"
					],
					"grams": {
					  "decimal string": "10.512",
					  "fraction string": "14794324775912079/1407374883553280"
					}
				  }
				}
			  }
			}
		  ],
		  "unites": []
		},
		{
		  "essential": {
			"includes": [],
			"names": [
			  "monounsaturated fat",
			  "fatty acids, total monounsaturated"
			],
			"region": 51
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per recipe": {
				"grams": {
				  "fraction string": "42204358007995761/1759218604441600"
				}
			  }
			}
		  },
		  "natures": [
			{
			  "amount": "1",
			  "source": {
				"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
				"FDC ID": "2663758",
				"UPC": "842234001664",
				"DSLD ID": ""
			  },
			  "ingredient": {
				"name": "Fatty acids, total monounsaturated"
			  },
			  "measures": {
				"mass + mass equivalents": {
				  "per package": {
					"listed": [
					  "23.990",
					  "g"
					],
					"grams": {
					  "decimal string": "23.990",
					  "fraction string": "42204358007995761/1759218604441600"
					}
				  }
				}
			  }
			}
		  ],
		  "unites": []
		}
	  ]
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "calcium",
		  "calcium, ca"
		],
		"region": 9
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "378/3125"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Calcium, Ca"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "120.960",
				  "mg"
				],
				"grams": {
				  "decimal string": "0.121",
				  "fraction string": "378/3125"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "cholesterol"
		],
		"region": 12
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "0"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Cholesterol"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "0.000",
				  "mg"
				],
				"grams": {
				  "decimal string": "0.000",
				  "fraction string": "0"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin a",
		  "vitamin a, iu"
		],
		"region": 16
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin e"
		],
		"region": 19
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "iodine"
		],
		"region": 29
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "magnesium"
		],
		"region": 30
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"accepts": [],
		"includes": [],
		"names": [
		  "Zinc, Zn",
		  "Zinc"
		],
		"region": 31
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "selenium"
		],
		"region": 32
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "copper"
		],
		"region": 33
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "manganese"
		],
		"region": 34
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "chromium"
		],
		"region": 35
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "molybdenum"
		],
		"region": 36
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "choline"
		],
		"region": 37
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "lutein"
		],
		"region": 40
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "betaine hydrochloride"
		],
		"region": 43
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "rutin"
		],
		"region": 44
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "strontium"
		],
		"region": 47
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vanadium"
		],
		"region": 49
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"accepts": [],
		"includes": [],
		"names": [
		  "Phosphorus, P",
		  "Phosphorus"
		],
		"region": 57
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "potassium",
		  "potassium K",
		  "potassium, K",
		  "K"
		],
		"region": 58
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "2439/3125"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Potassium, K"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "780.480",
				  "mg"
				],
				"grams": {
				  "decimal string": "0.780",
				  "fraction string": "2439/3125"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "sodium Na",
		  "sodium, Na",
		  "sodium",
		  "Na"
		],
		"region": 59
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "27/25"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Sodium, Na"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "1080.000",
				  "mg"
				],
				"grams": {
				  "decimal string": "1.080",
				  "fraction string": "27/25"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin d",
		  "vitamin d (d2 + d3)",
		  "vitamin d2",
		  "vitamin d3",
		  "vitamin d (d2 + d3), international units"
		],
		"region": 63
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "0"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Vitamin D (D2 + D3)"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "0.000",
				  "µg"
				],
				"grams": {
				  "decimal string": "0.000",
				  "fraction string": "0"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin c",
		  "vitamin c (form: as calcium ascorbate)",
		  "vitamin c, total ascorbic acid"
		],
		"region": 64
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin k",
		  "vitamin k1",
		  "vitamin k2"
		],
		"region": 65
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "thiamin",
		  "thiamine",
		  "vitamin b1"
		],
		"region": 66
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "riboflavin",
		  "vitamin b2"
		],
		"region": 67
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin b3",
		  "niacin",
		  "nicotinamide",
		  "nicotinamide riboside"
		],
		"region": 68
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin b5",
		  "pantothenic acid"
		],
		"region": 69
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"accepts": [
		  "pyridoxine",
		  "pyridoxal",
		  "pyridoxamine"
		],
		"includes": [],
		"names": [
		  "Vitamin B6",
		  "Vitamin B-6"
		],
		"region": 70
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin b7",
		  "biotin",
		  "vitamin h"
		],
		"region": 71
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "vitamin b9",
		  "folate",
		  "folacin"
		],
		"region": 72
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"accepts": [],
		"includes": [],
		"names": [
		  "Vitamin B12",
		  "Vitamin B-12",
		  "cobalamin"
		],
		"region": 73
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "calories",
		  "energy"
		],
		"region": 74
	  },
	  "measures": {
		"energy": {
		  "per recipe": {
			"food calories": {
			  "fraction string": "14976/25"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Energy"
		  },
		  "measures": {
			"energy": {
			  "per package": {
				"listed": [
				  "599.040",
				  "kcal"
				],
				"food calories": {
				  "decimal string": "599.040",
				  "fraction string": "14976/25"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"includes": [],
		"names": [
		  "iron",
		  "iron, fe"
		],
		"region": 75
	  },
	  "measures": {
		"mass + mass equivalents": {
		  "per recipe": {
			"grams": {
			  "fraction string": "35871171032006001/7036874417766400000"
			}
		  }
		}
	  },
	  "natures": [
		{
		  "amount": "1",
		  "source": {
			"name": "F'SH PLANT-BASED TENDER, FLAKY FILETS IN A LIGHT GOLDEN TEMPURA BATTER, F'SH",
			"FDC ID": "2663758",
			"UPC": "842234001664",
			"DSLD ID": ""
		  },
		  "ingredient": {
			"name": "Iron, Fe"
		  },
		  "measures": {
			"mass + mass equivalents": {
			  "per package": {
				"listed": [
				  "5.098",
				  "mg"
				],
				"grams": {
				  "decimal string": "0.005",
				  "fraction string": "35871171032006001/7036874417766400000"
				}
			  }
			}
		  }
		}
	  ],
	  "unites": []
	},
	{
	  "essential": {
		"accepts": [],
		"includes": [],
		"names": [
		  "Folate, DFE"
		],
		"region": 76
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	},
	{
	  "essential": {
		"accepts": [],
		"includes": [],
		"names": [
		  "Folate",
		  "Folate, Total"
		],
		"region": 77
	  },
	  "measures": {},
	  "natures": [],
	  "unites": []
	}
  ]
}
