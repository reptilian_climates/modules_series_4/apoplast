


def my_decorator(func):
	def wrapper (* args, ** kwargs):
		print ("before", args)
		result = func (*args, **kwargs)
		print ("after")
		return result
	return wrapper
	
def check_opener (func):
	def wrapper (* args, ** kwargs):
		print ("check_opener", args)
		result = func (*args, **kwargs)
		print ("after")
		return result
		
	return wrapper
	
def prevent_call(func):
	def wrapper(*args, **kwargs):
		raise Exception("Function call is prevented by the decorator")
		# Alternatively, you can perform some other action here, like logging
		# print("Function call is prevented by the decorator")
	return wrapper
	
def prevent_call2(func):
	def wrapper(*args, **kwargs):
		print ('preventing')
		return None
	return wrapper

@my_decorator
@check_opener
@prevent_call2
def say_hello (name):
	print (f"Hello, {name}!")

# Calling the decorated function with parameters
say_hello ("John")

print ('done')
