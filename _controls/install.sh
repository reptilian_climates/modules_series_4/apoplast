
#
#	sh /habitat/_controls/install.sh
#

#. /habitat/.venv/bin/activate
#python3 /habitat/_controls/install_with_uv.py

pip install uv

rm -rf /habitat/.venv
rm /habitat/requirements.txt

uv venv
. /habitat/.venv/bin/activate

(cd /habitat && uv pip compile pyproject.toml -o requirements.txt)
uv pip sync requirements.txt




